class TestWebComponent extends HTMLElement {

  constructor() {
    super()

    this.template = `
      <h3>Manage push notifications</h3>
    `
  }

  connectedCallback() {
    this.innerHTML = this.template
  }

  registerStateChange(changeEvent) {
    if (changeEvent.type !== "APP_CHANGE") {
      return
    }
    this.innerHTML = `
      <h3>Manage push notifications</h3>
      <div>
        <emph>Appname</emph>:<strong>${changeEvent.payload}</strong>
      </div>
    `
  }

  registerCallback() {

  }
}

module.exports = TestWebComponent