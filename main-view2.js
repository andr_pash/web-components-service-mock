class TestWebComponent extends HTMLElement {
  constructor() {
    super()

    this.template = `
      <h3>Import and export CSV data</h3>
    `
  }

  connectedCallback() {
    this.innerHTML = this.template
  }

  registerStateChange(changeEvent) {
    if (changeEvent.type !== "APP_CHANGE") {
      return
    }
    
    this.innerHTML = `
      <h3>Import and export CSV data</h3>
      <div>
        <emph>Appname</emph>:<strong>${changeEvent.payload}</strong>
      </div>
    `
  }
}

module.exports = TestWebComponent