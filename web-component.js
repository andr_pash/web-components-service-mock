class TestWebComponent extends HTMLElement {
  constructor() {
    super()

    this.template = `
      <div>Push</div>
    `
  }

  connectedCallback() {
    this.innerHTML = this.template
  }

  registerStateChange(changeEvent) {
    console.log('custom web component received state change', changeEvent)
  }
}

module.exports = TestWebComponent