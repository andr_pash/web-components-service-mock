class TestWebComponent2 extends HTMLElement {
  constructor() {
    super()

    this.template = `
      <div>CSV</div>
    `
  }

  connectedCallback() {
    this.innerHTML = this.template
  }

  registerStateChange(changeEvent) {
    console.log('custom web component received state change', changeEvent)
  }
}

module.exports = TestWebComponent2